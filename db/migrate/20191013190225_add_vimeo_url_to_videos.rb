class AddVimeoUrlToVideos < ActiveRecord::Migration[5.2]
  def change
    add_column :videos, :vimeo_url, :string
  end
end
