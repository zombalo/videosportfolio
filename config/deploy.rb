# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "videosportfolio"
set :repo_url, "git@bitbucket.org:zombalo/videosportfolio.git"
set :ssh_options, { :forward_agent => true }

set :user, 'deployer'

server '46.29.167.28', user: 'deployer', roles: %w{web app db}
set :nginx_server_name, 'zorincom.ru'

set :deploy_to, "/home/deployer/videosportfolio"
set :branch, 'master'

set :nginx_use_ssl, true
set :nginx_ssl_cert_local_path, '/Users/zombalo/ssl/zorincom.ru.crt'
set :nginx_ssl_cert_key_local_path, '/Users/zombalo/ssl/zorincom.ru.key'

set :use_sudo, false
set :stage, :production
set :rvm_custom_path, '/usr/share/rvm'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

# Default value for linked_dirs is []
append :linked_dirs, "storage"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
