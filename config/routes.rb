Rails.application.routes.draw do
  root 'main#index'
  get 'main/show_video/:id', to: 'main#show_video', as: 'show_video'
  post 'main/ask_question'

  resources :videos
end
