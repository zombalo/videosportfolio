class AskMailMailer < ApplicationMailer
  default to: 'eco369@mail.ru'

  def ask_mail
    @sender = params[:sender]

    mail(subject: 'Вопрос с сайта', from: @sender[:email])
  end
end
