class VideosController < ApplicationController
  http_basic_authenticate_with name: "eco369", password: "369369369"
  layout 'admin'

  def index
    @videos = Video.all
  end

  def new
    @video = Video.new
  end

  def create
    @video = Video.new(video_params)

    if @video.save
      flash[:success] = 'Видео успешно создано'
      redirect_to videos_path
    else
      render 'new'
    end
  end

  def edit
    @video = Video.find(params[:id])
  end

  def update
    @video = Video.find(params[:id])
    @video.update(video_params)

    if @video.save
      flash[:success] = 'Видео успешно изменено'
      redirect_to videos_path
    else
      render 'edit'
    end
  end

  def destroy
    Video.find(params[:id]).destroy
  end

  private
  def video_params
    params.require(:video).permit(:video_type, :title, :description, :video_file, :preview_file, :vimeo_url)
  end
end