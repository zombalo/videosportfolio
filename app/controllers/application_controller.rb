class ApplicationController < ActionController::Base
  helper_method :video_preview

  private
  def video_preview video
    if video.preview_file.attached? then video.preview_file else video.video_file.preview(resize: "500x500") end
  end
end
