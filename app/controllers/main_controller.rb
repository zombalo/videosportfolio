class MainController < ApplicationController
  def index
  end

  def show_video
    @video = Video.find(params[:id])
  end

  def ask_question
    AskMailMailer.with(sender: params[:sender]).ask_mail.deliver_now
  end
end
