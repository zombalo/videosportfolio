var showModal = function(content)
{
  var modal = $('#modal');
  modal.find('.content').append($(content));
  $('#body-overlay').removeClass('d-none');
  modal.removeClass('d-none');
};

var hideModal = function()
{
  var modal = $('#modal');
  $('#body-overlay').addClass('d-none');
  modal.addClass('d-none');
  modal.find('.content').empty();
};

$(document).on('turbolinks:load', function(){
  $('#body-overlay').click(hideModal);
  $('body').on('click', '#modal .cross-side i', hideModal);

  var interval = 150;
  var timeout = 0;

  $('#head-section .pics img').each(function(){
    var pic = $(this);
    setTimeout(function(){
      pic.removeClass('preshow');
    }, timeout);
    timeout += interval;
  });

  var interval = 100;
  var timeout = 0;

  $('#head-section .ltr').each(function(){
    var ltr = $(this);
    setTimeout(function(){
      ltr.removeClass('preshow');
    }, timeout);
    timeout += interval;
  });
});