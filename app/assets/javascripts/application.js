// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery
//= require popper
//= require bootstrap-sprockets
//= require activestorage
//= require_tree .

$(document).on('turbolinks:load', function(){
  $(window).scroll(onScroll);
  onScroll();

  $('.contact-form .side .ask').click(function(){
    $(this).closest('.contact-form').addClass('active');
  });

  $('.contact-form .side .close-ask').click(function(){
    $(this).closest('.contact-form').removeClass('active');
  });
});

var onScroll = function(){
  var elements = $('.preshow');
  var showMargin = $(window).scrollTop() + ($(window).height());

  elements.each(function(){
    if($(this).offset().top <= showMargin)
      $(this).removeClass('preshow');
  });
}

var showNotify = function(message)
{
  var notify = $('#notify');
  notify.text(message);

  notify.addClass('active');

  setTimeout(function(){
    notify.removeClass('active');
  }, 1400);
};