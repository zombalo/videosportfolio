class Video < ApplicationRecord
  has_one_attached :video_file
  has_one_attached :preview_file

  validates :video_type, presence: true
  validates :title, presence: true
  # validates :description, presence: true

  validate :video_validate
  validate :preview_validate

  enum video_type: [ 'Реклама', 'Торжества', 'Семейное']

  def video_validate
    if !video_file.attached? && !vimeo_url.present?
      errors.add(:video_file, 'Нужно добавить видео файл');
    end
  end

  def preview_validate
    if !preview_file.attached?
      errors.add(:preview_file, 'Нужно добавить файл превью');
    end
  end
end
